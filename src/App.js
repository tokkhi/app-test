import React from "react";
import Home from './component/Home';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { Container } from "react-bootstrap";
    
       
     function App()  {
  
  return (
    <Container>
      <Router>
      <Switch>
        <Route path="/">
            <Home />
        </Route>
      </Switch>
      </Router>   
  </Container>
  );
}

export default App;
