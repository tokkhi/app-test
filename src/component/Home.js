import React, { Component } from 'react';
import axios from 'axios'

var listData = []
class Home extends Component{
  
    constructor(props){
        super(props)
        this.listData = []
         this.config = {
          headers: {'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'}
      };
      
      this.handleChange = this.handleChange.bind(this)
      this.keyPress = this.keyPress.bind(this)
      this.update = this.update.bind(this)
      this.selectbox.bind(this)

      }
      state = {
        listData: [],
        value: "",
        edit: [],
        data:[],
        cb:[]
      }
      
      
      componentDidMount() {
      axios.get('/test').then(res=>{
        this.setState({ listData: res.data.data });
      })
      };

      handleChange(e) {
        this.setState({[e.target.name]: e.target.value });
      }

      handleChangeList(e) {
        let data = this.state.data;
        data[e.target.name] = e.target.value;
        this.setState({data});
       
      }
     
     keyPress(e){
      if(e.keyCode === 13){
        axios.post('/test/create', {
          data: e.target.value
        },this.config).then(res=>{
          console.log(res)
          this.componentDidMount()
        })
        
      }
         
      
   }
   delete(data){
    axios.delete('/test/delete/'+data.id,this.config).then(res=>{
      this.componentDidMount()
    })
   }

   edit(e){
     console.log(e)
     let edit = this.state.edit.slice();
      edit[e.id] = !edit[e.id];
      this.setState({edit});
      let data = this.state.data.slice();
      data[e.id] = e.data;
      this.setState({data});
   }

   update(e){
     if(e.keyCode === 13){
      axios.put('/test/update/'+e.target.name,
      {data: e.target.value},this.config).then(res=>{
        
        let edit= []
        this.setState({edit});
        this.componentDidMount()
      })
     }

  }
  selectbox(e){
    let cb = this.state.cb;
    cb[e.id] = !cb[e.id];
    this.setState({cb});

    var x = document.getElementById('data'+e.id);
    if(cb[e.id]){
      x.style.textDecorationLine = 'line-through';
      x.style.color = "#DCDCDC";
    }else{
      x.style.textDecorationLine = null;
      x.style.color = "#000000";
    }
    
  }


    render(){
      const mystyle = {
        margin: "20px",
        padding: "10px"
      };
        return(
          <div class="container card text-center" style={mystyle}>
          <h1>My Todo</h1>
          <input type="text" name="value" class="form-control" placeholder="input data to add" aria-label="Input" aria-describedby="basic-addon1" value={this.state.value} onKeyUp={this.keyPress} onChange={this.handleChange}/>
          
          <div class="top-buffer">
          <ul class="list-group">
            {this.state.listData.map(fbb =>
    
                 <li key={fbb.id} class="list-group-item">
                   <div class="row">
                  <div class="col-1 text-left">
                  <input type="checkbox" value={this.state.cb[fbb.id]} onChange={() => this.selectbox(fbb)}></input>
                  </div>
                  <div class="col-6 text-left">
                  {this.state.edit[fbb.id] ? 
                   <span key={'data'+fbb.id} name={fbb.id}>
                   <input name={fbb.id} type="text" class="form-control" placeholder="data" aria-label="data" onKeyUp={this.update.bind(fbb)} aria-describedby="basic-addon1" value={this.state.data[fbb.id]}onChange={this.handleChangeList.bind(this)}/>
                    </span> : 
                    <span key={'data'+fbb.id} id={'data'+fbb.id}>
                      {fbb.data} 
                      
                    </span>
                    }
                  </div>
                  <div class="col-sm text-right">

                    <span class="col-sm" onClick={() => this.edit(fbb)}>
                   <i class="fas fa-pen" ></i>
                   </span>
                    <span onClick={() => this.delete(fbb)}>
                   <i class="fas fa-trash-alt"></i>
                   </span>
                    
                   
                 
                  </div>
                  </div>
                  
                   
                    
                   
                   
     </li>
                  )}
          </ul>
          </div>
            
          </div>

        )
    }
} 

export default Home;